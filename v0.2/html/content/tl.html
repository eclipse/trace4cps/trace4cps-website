<!--

    Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation

    This program and the accompanying materials are made
    available under the terms of the Eclipse Public License 2.0
    which is available at https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5.0 Transitional//EN">

<html>
<head>
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>TRACE - Runtime Verification</title>
</head>
<div>

<h1>TRACE Runtime verification</h1>
<p>
The TRACE tool provides a domain-specific language to formally specify properties about
traces.
Given such a formal specification and a trace, the TRACE tool can
compute whether the trace satisfies the specification.
This is a form of (offline) runtime verification.
Such verification is broadly applicable: from model validation and verification
to diagnosis of problems that occur in a deployed system. 
</p>
<p>
The specifications are stored in plain text files with the <tt>etl</tt> extension.
When such files are opened in the Eclipse UI (with the TRACE feature installed), a
dedicated editor is opened with syntax highlighting and support for auto-completion. 
</p>
<p>
Note that TRACE currently does not have algorithms for online runtime verification.
</p>
<h2>Specifying properties</h2>
<p>
The language and algorithms are based on <i>temporal logic theory</i> and work on events,
claims and signals; dependencies are out of scope.
Properties about events and claims can be specified and verified using
metric temporal logic (MTL) techniques.
Signal temporal logic (STL) can be used to specify and verify properties of signals.
These can also be mixed resulting in a top-level MTL formula with STL and MTL subformulas.
</p>

<h3>MTL atomic propositions</h3>
At the basis of MTL formulas are the so-called atomic propositions which are key-value mappings,
with an optional indication whether the atomic
proposition concerns the start or end of a claim.
There is a straightforward evaluation function that indicates whether an atomic proposition
is satisfied by a claim or event.
This function just checks whether every key-value pair in the atomic proposition is also present
in the key-value pairs of the claim or event.
The MTL atomic propositions are given by the following syntax: 

<div class="code">MtlAp: ('start' | 'end')? AttributeFilter

AttributeFilter : '{' KeyVal (',' KeyVal)* '}'

KeyVal: IdString '=' IdString

IdString: STRING | ID |
            STRING '+' ID | ID '+' STRING | STRING '+' ID '+' STRING;
</div >

For example, the following are MTL atomic propositions:

<div class="code2">{ 'name'='A' }

{ 'name'='A', 'id'=i }

end { 'name'='A' }
</div >
<p>
The first atomic proposition is satisfied by any claim or event with an attribute 'name' with value 'A'.
The third is satisfied only by end events of claims which have a 'name' attribute with value 'A'.
The second atomic proposition is <i>parameterized</i> and
its satisfaction depends on the value of the parameter <tt>i</tt>.
Below we explain how such parameters are defined.
Note that the '+' in the 'IdString' rule means concatenation of strings.
</p>

<h3>STL atomic propositions</h3>
<p>
At the basis of STL formulas are STL atomic propositions.
An STL atomic proposition is a reference to a signal definition, possibly taking the time derivative,
and a comparison with a real number.
A signal definition consists of a unique identifier, and a signal specification.
A signal specification can take several forms:
<ul>
<li>An attribute filter can be used to select a single signal from the trace (if the filter matches
multiple signals, then an error is raised).</li>
<li>A throughput, latency or wip signal can be derived from the trace data (see <a href="ll.html">here</a>) and used
in an STL atomic proposition.
Note that the throughput is either based on an identifier, or on an MTL atomic proposition which specifies a subset
of events to compute the throughput from.
</li>
<li>A resource-usage signal (the claimed amount or the number of concurrent clients on a resource)
can be derived from the trace data (see <a href="resource.html">here</a>) and used
in an STL atomic proposition.</li>
</ul>
In the latter two types, the derived signals, the
signal can be modified with a convolution specification.
This computes a signal that gives a sliding-window average of the original signal.
</p>
<p>
STL atomic propositions are defined with the following syntax:
</p>

<div class="code">StlAp: ([SignalDef] | 'd' [SignalDef] '/' 'dt') CompOp DOUBLE_T

SignalDef : 'signal' name=ID ':' Signal

Signal:
    AttributeFilter
    |
    'throughput' 'of' (ID | MtlAp) ('per' TimeUnit)? ConvSpec?
    |
    'latency' 'of' ID ('in' TimeUnit)? ConvSpec?
    |
    'wip' 'of' ID ConvSpec?
    |
    'resource-amount' AttributeFilter ConvSpec?
    |
    'resource-clients' AttributeFilter ConvSpec?;

ConvSpec: 'over' DOUBLE_T TimeUnit;

CompOp: '<=' | '==' | '>='

TimeUnit: 's' | 'ns' | 'us' | 'ms' | 'min' | 'hr'
</div>

<p>
Some examples of signal definitions are below:
<div class="code2">signal s1 : {'name'='signal 1'}

signal tp_hr_10s : throughput of id per hr over 10.0 s

signal ram_usage : resource-amount {'resource name'='RAM', 'unit'='MB'}
</div >
The first signal definition refers to a signal from the trace data which has an attribute 'name'
with value 'signal 1'. If this filter does not match exactly one signal, then an error is raised.
The second signal definition is the derived identifier-based throughput signal for the identifier
'id'. The throughput is given in units per hour, and averaged over a sliding window of 10 seconds.
The third definition computes the instantaneous resource usage of the resource with attributes
'resource name' and 'unit' with values 'RAM' and 'MB' respectively. 
Using these signal definitions, we can specify the following STL atomic propositions:
<div class="code2">d s1 / dt <= 23.3

tp_hr_10s >= 100.0

ram_usage <= 256
</div >
Note that the first atomic proposition takes the time derivative of signal s1.
</p>

<h3>Temporal-logic formulas, definitions and checks</h3>
<p>
Using the atomic propositions as building blocks, we can specify more complicated formulas.
There are two top-level concepts for this: (i) definition, and (ii) checks.
Definitions are abbreviations for (parameterized) formulas.
Checks are used to specify formulas that need to be verified.
Since we can have parameters in MTL atomic propositions and definitions,
a check has an optional 'forall' part that specifies the name and
domain of an integer parameter. Clearly, if the formula is parameterized,
then the forall construct must be used to select parameter values.
</p>
Formulas, that can be used in definitions and checks, are based on the MTL and STL
atomic propositions explained above. These building blocks can be combined with
the following constructs:
<ul>
<li><i>not &phi;</i> : creates the negation of <i>&phi;</i></li>
<li><i>if &phi;<sub>1</sub> then &phi;<sub>2</sub></i>, <i>&phi;<sub>1</sub></i> and <i>&phi;<sub>2</sub></i>,
<i>&phi;<sub>1</sub> or &phi;<sub>2</sub></i> : creates the boolean combination of the two formulas</li>
<li><i>globally &phi;</i> : states that <i>&phi;</i> holds always from now</li>
<li><i>finally &phi;</i> : states that <i>&phi;</i> eventually holds starting from now</li>
<li><i>during I &phi;</i> : states that <i>&phi;</i> holds during the specified time interval <i>I</i>,
relative to now</li>
<li><i>within I &phi;</i> : states that <i>&phi;</i> holds somewhere in the specified time interval <i>I</i>,
relative to now</li>
<li><i>until &phi;<sub>2</sub> we have that &phi;<sub>1</sub></i> :
states that <i>&phi;<sub>1</sub></i> holds from now until <i>&phi;<sub>2</sub></i> holds
</li>
<li><i>by I &phi;<sub>2</sub> and until then &phi;<sub>1</sub></i> :
states that <i>&phi;<sub>1</sub></i> holds until <i>&phi;<sub>2</sub></i> holds and that
<i>&phi;<sub>2</sub></i> holds within <i>I</i> time units from now
</li>
</ul>
<p>
Furthermore, a (parameterized) reference to a definition is also a formula.
This all is specified by the following syntax.
</p>
<div class="code">Def: 'def' ID ('(' ID ')')? ':' Formula

Check: 'check' ID ':' ('forall' '(' ID ':' INT_T '...' INT_T ')')? Formula

Formula:
  MtlAp
  |
  StlAp
  |
  'not' Formula'
  |
  'if' Formula 'then' Formula
  |
  '(' Formula ('and' | 'or') Formula ')'
  |
  'globally' Formula
  |
  'finally' Formula
  |
  'during' Interval Formula
  |
  'within' Interval Formula
  |
  'until' Formula 'we' 'have' 'that' Formula
  |
  'by' Interval Formula 'and' 'until' 'then' Formula
  |
  [Def] ('(' ID ('+' INT_T)? ')')?
  
Interval:
  '(' DOUBLE_T ',' (DOUBLE_T | 'Infty') ')' TimeUnit
  |
  '(' DOUBLE_T ',' DOUBLE_T ']' TimeUnit
  |
  '[' DOUBLE_T ',' (DOUBLE_T | 'Infty') ')' TimeUnit
  |
  '[' DOUBLE_T ',' DOUBLE_T ']' TimeUnit
</div>

<p>
Formulas that only contain MTL atomic propositions are MTL formulas.
Similarly, formulas  that only contain STL atomic propositions are STL formulas.
Our variant of STL has the limitation that the general until (the <tt>until...</tt> and <tt>by...</tt>
constructions above) is not supported. If these constructions are used with two STL subformulas, then
the resulting formula will be an MTL formula.
Finally, if a formula that uses two subformulas is used, and one of the subformulas is MTL and the
other is STL, then the formula will be an MTL formula. In that case, the signal of the STL formula
is sampled on the timestamps at which events take place. This can cause unexpected semantic effects.
The final section below explains this by an example. For details we refer to [5] of the
<a href="references.html">references</a>.
</p>

<p>
The syntax above allows us to specify, for instance, the following definitions:
<div class="code2">def start_or_end_of_claim_A : { 'name'='A' }

def start_or_end_of_claim_A_instance(i) : { 'name'='A', 'id'=i }

def end_claim_A : end { 'name'='A' }

signal tp_hr_10s : throughput of id per hr over 10.0 s

def good_productivity : tp_hr_10s >= 20.0
</div >
These definitions are formulas themselves and can be used as such.
Examples of checks are the following:
<div class="code2">check c1 : finally start_or_end_of_claim_A

check c2 : forall ( i : 0 ... 9 ) finally start_or_end_of_claim_A_instance(i)
</div>
The first check is true if the trace contains at least one event with attribute 'name' 
and value 'A'. 
The second check is true if the trace contains at least one event with attribute 'name' 
with value 'A', and attribute 'id' with value <i>i</i> for all <i>0 &le; i &le; 9</i>.
</p>
<p>
Note that the definitions can be used to decompose complicated specifications.
By naming them properly, the top-level specification, the check, can be made
comprehensible for non-experts.
Of course, the specification itself and the decomposition still is specialized work.
</p>
Let us now consider the <a href="example.html">running example</a>. We have an execution
trace for the processing of 200 objects that only contains claims with two attributes:
the name of the processing step and
the identifier (an integer) of the object that is being processed.
We specify that the object processing latency is at most 50 ms as follows.

<div class="code2">def processing_starts(i) : start { 'name'='A', 'id'=i}

def processing_ends(i) : end {'name'='G', 'id'=i}

check latency_antecedent : forall (i : 0 ... 199)
                            finally processing_starts(i)

check latency_discrete : forall (i : 0 ... 199)
                            globally
                                if processing_starts(i) then
                                  within [0.0, 50.0) ms processing_ends(i)
</div>
Note that the 'latency_antecedent' check is a sanity check that ensures that the antecedent
of the implication in 'latency_discrete' actually occurs in the trace. 
This example shows that by choosing proper labels for definitions, the resulting
checks can be rather readable.
</p>
<p>
The specifications above are all based on MTL. Below follow examples using STL:

<div class="code2">signal tp_per_s : throughput of id per s over 10.0 ms

signal work_in_progress : wip of id

signal latency_ms : latency of id in ms

def work_done_within_100ms : during [100.0, Infty) ms work_in_progress == 0.0

check latency_cont : globally latency_ms <= 50.0

check pipeline_depth : within [0.0, 100.0) ms
                            globally
                                ( work_in_progress >= 3.0 or work_done_within_100ms )

check productivity: within [0.0, 100.0) ms
                        globally
                            ( tp_per_s >= 100.0 or work_done_within_100ms ) 
</div>
The first check is based on the continuous latency signal and states that the latency
is never larger than 50 ms.
The second check states that after 100 ms of start-up time there are always at
least 3 images being processed by the system, except when the system is stopping.
This pattern to deal with transient behavior during start up and shut down is also
used in the third check to state that the steady-state throughput is at least 100
frames per second. 
</p>

<h2>A method for creating formulas</h2>
Creating proper formulas is generally not straightforward.
Below we describe a top-down method to systematically develop a formula.

<h3>1. Write down the top-level formula in semi-natural language</h3>
The first step is to write down the formula that needs to be checked in semi-natural
langage. Try to formulate it using the following constructs:
<ul>
<li>It must always hold that ... / during [] it always holds that ...</li>
<li>Eventually ... happens / within [] ... happens</li>
<li>Until ... we have that ... / by [] ... and until then ... </li>
</ul>
Here the ... are so-called sub-formulas that can be defined later.
The [] indicate time intervals. Note that the second variations in the items above
are specializations of the first versions; they add a specific time constraint.
It usually helps if you can write the sub-formulas also in semi-natural language.
Examples are the following:
<ol>
<li>It must always hold that the throughput is at least 100 wafers per hour.</li>
<li>Within 10 seconds a wafer change happens.</li>
<li>Until measurements have been done we have that no pickup activity happens.</li>
</ol>

Using the specification language, these sentences can be partially formalized.
Below is the formalization of the first check. Note that we have applied the globally operator,
and introduced a definition (which still is not complete) for the sub-formula about
the throughput.

<div class="code">def throughput_is_at_least_than_100_wafers_per_hour : ...

check c1 : globally throughput_is_at_least_than_100_wafers_per_hour
</div>

The formalization of the second check is as follows
(note the formalization of the time interval):

<div class="code">def a_wafer_change_happens : ...

check c2 : within [0.0, 10.0] s a_wafer_change_happens
</div>

The third check is formalized as follows:

<div class="code">def no_pickup_activity_happens : ...

def measurements_have_been_done : ...

check c3 : until measurements_have_been_done we have that no_pickup_activity_happens
</div>

<h3>2: Complete definitions</h3>
The top-level checks have been defined now in terms of definitions that are not yet formalized.
This step makes these definitions formal by repeatedly applying it until no informal
definitions are left.
Again, we first decompose with semi-natural language. We can use the following patterns:
<ul>
<li>Event ... occurs</li>
<li>Signal ... has value larger/smaller/equal than/to ...</li>
<li>If ... then ...</li>
<li>... and ...</li>
<li>... or ...</li>
<li>not/no ...</li>
<li>It must always hold that ... / during [] it always holds that ...</li>
<li>Eventually ... happens / within [] ... happens</li>
<li>Until ... we have that ... / by [] ... and until then ... </li>
</ul>
We use the third check above as an example:

<div class="code">def no_pickup_activity_happens : <b>no pickup event occurs</b>

def measurements_have_been_done : <b>a measurement done event occurs and within 10 ms another such event occurs</b>

check c3 : until measurements_have_been_done we have that no_pickup_activity_happens
</div>

We first formalize that "no pickup event occurs". We introduce another definition
for the pickup event (a MTL atomic proposition), and use the not construction:

<div class="code"><b>def pickup_event : { 'name'='pickup' }</b>

def no_pickup_activity_happens : <b>not pickup_event</b>

def measurements_have_been_done : a measurement done event occurs and within 10 ms another such event occurs

check c3 : until measurements_have_been_done we have that no_pickup_activity_happens
</div>

The measurements_have_been_done definition then is formalized as follows.

<div class="code">def pickup_event : { 'name'='pickup' }

def no_pickup_activity_happens : not pickup_event

<b>def measurement_done_event : { 'name'='measure', 'type'='done' }</b>

def measurements_have_been_done : <b>( measurement_done_event and within (0.0, 10.0] ms measurement_done_event )</b>

check c3 : until measurements_have_been_done we have that no_pickup_activity_happens
</div>

Note the subtlety with respect to the interval <tt>(0.0, 10.0]</tt>, which is a left-open
interval. This is needed to exclude the first measurement event.
A single measurement event would satisfy the <tt>measurements_have_been_done</tt>
formula if the interval is left-closed, i.e., <tt>[0.0, 10.0]</tt>.

<p>
Although this specification is complete, it is not exactly the right one.
It does not exclude the situation where a pickup event happens between the
two measurement events.
This is allowed, because the <tt>measurements_have_been_done</tt> formula
is true at the moment of the first of the measurement events.
This is the nature of the temporal operators, that can only look into the future.
To fix this, we need to exclude pickup activity also between the two measurement events.
We therefore change the "within" operator to the "until" operator, and rename our
definition slightly:

<div class="code">def pickup_event : { 'name'='pickup' }

def no_pickup_activity_happens : not pickup_event

def measurement_done_event : { 'name'='measure', 'type'='done' }

<b>def measurement_without_pickup_activity :
   ( measurement_done_event
   and
   by (0.0, 10.0] ms measurement_done_event and until then no_pickup_activity_happens )</b>

check c3 : until
               <b>measurement_without_pickup_activity</b>           
           we have that
               no_pickup_activity_happens
</div>
</p>

<h3>3: Rewrite to improve readability</h3>
The final step is to rewrite the definitions to improve the readability.
Sometimes it helps to introduce additional definitions, or to merge definitions.
Also naming the definitions in a way such that semi-natural language appears is
a good way to increase readability and convey the ideas to others.

<h2>Verifying execution traces</h2>
<p>
Above we have briefly explained how specifications can be written.
Now we show how we can verify such a specification on a given trace in the UI.
This can be done by invoking the <img src="../../icons/passed.png" /> menu item on
a trace view, which
opens a dialog to select a specification file with the <tt>etl</tt> extension from the
Eclipse workspace.
All checks from the specification are verified and the results are shown in a separate view,
see Figure 1.
</p>

<figure>
<a target="_blank" href="../img/etl-result.png"><img src="../img/etl-result.png" border="1"/> </a>
<figcaption>Figure 1: The Etl result view.</figcaption>
</figure>

<p>
The view keeps track of all trace file and specification combinations that have been checked.
If files are modified, then results relating to these files are removed from the result view.
Note that a check can have three kinds of results: GOOD, BAD or NON_INFORMATIVE.
The last result is only possible when a pure MTL formula is checked (i.e., no continuous signals
are used). Then the trace is interpreted as being a prefix of a longer trace that we don't know yet.
The NON_INFORMATIVE result then means that from the prefix we cannot decide whether any extension
of the prefix will satisfy or dissatisfy the formula.
For more information on this <i>informative prefix semantics</i> of MTL we refer to [3] of the
<a href="references.html">references</a>.
</p>
<p>
Double-clicking a BAD check in the result view adds claims and/or events to the trace to explain why
the check is not satisfied.
The validity of the check element and each def element that is used in the check is shown either as
a colored bar (in case of an STL formula) or as a number of colored events (in case of a pure MTL formula).
These elements are either green or red, indicating the validity of the check or def at that point in time.
For instance, Fig. 2 shows the explanation of the <tt>productivity</tt> check of the running example that
is defined as follows:
<div class="code">signal work_in_progress : wip of id 

signal tp_per_s : throughput of id per s over 10.0 ms

def work_done_within_100ms : during [100.0, Infty) ms work_in_progress == 0.0

def good_throughput: ( tp_per_s >= 100.0 or work_done_within_100ms )

check productivity: within [0.0, 100.0) ms
                        globally good_throughput
</div>
</p>
The two defs and single check are shown, and clearly the check is not satisfied because
there is no continuous <tt>good_throughput</tt> after 100 ms. This is shown by the red
interruptions in the <tt>good_throughput</tt> section, which indicates that at those points in time
the throughput is not good (i.e, at least 100 objects per second).

<figure>
<a target="_blank" href="../img/etl-explanation.png"><img class="open600" src="../img/etl-explanation.png" border="1"/> </a>
<figcaption>Figure 2: Explanation of unsatisfied checks.</figcaption>
</figure>

<h2>Mixing MTL and STL formulas</h2>
<p>
Although the examples above either are pure MTL or pure STL formulas, the MTL and STL
concepts can be mixed. This allows specification of properties based on both discrete events
and continuous signals.
Care has to be taken, however, as the <i>mixed</i> semantics can have counter-intuitive effects.
</p>

<figure>
<a target="_blank" href="../img/mixed-ex.png"><img class="open600" src="../img/mixed-ex.png" border="1"/> </a>
<figcaption>Figure 3: Small example to illustrate mixed semantics.</figcaption>
</figure>

<p>
Consider the execution trace in Figure 3. It consists of two events and a continuous signal.
We have the following specification:
<div class="code">def red : {'name'='E', 'color'='red'}
def green : {'name'='E', 'color'='green'}
signal s1 : {'signal'='S1'}

check c1 : globally
                if s1 >= 1.5 then finally green

check c2 : globally
                if s1 <= 0.5 then finally red

check c3 : globally
                if s1 <= 0.000005 then finally red</div>
</p>

<p>
The two definitions and the signal declaration are used to refer to the red event, green event and the
blue signal respectively.
Note that all three checks use both MTL and STL atomic propositions.
Now look at check c1. It states that it always holds that if the blue signal is at least 1.5, then a
green event eventually happens.
On the interval [1.5, 3.5] we have that the antecedent of the implication holds. Therefore, the consequent
(a green event) must happen after time 1.5. Clearly this is true.
Indeed, verification shows that property c1 holds on the trace.
</p>

<p>
Now look at check c2.
It states that it always holds that if the blue signal is at most 0.5, 
then a red event eventually happens.
The antecedent is true between 4.5 and 5.5 seconds, but no red event follows.
Thus, the property should not hold.
Indeed, verification shows that property c2 does not hold on the trace.
</p>

<p>
Finally, consider check c3.
This check only differs from c2 in the threshold of the signal. Since the signal reaches value
0 at time 5, the antecedent of the implication is true in a small time interval around 5 seconds.
Thus, the property should not hold.
Verification, however, shows that property c3 holds on the trace, which is not as expected.
</p>

<p>
The reason that the verification algorithm concludes that c3 holds is the following.
Since a mixed formula is used, the STL subformulas are <i>sampled</i> at the points in time
where events occur. Our trace has only two events on the interval [2,8].
Clearly, the events in the discrete part can be very sparse, and therefore the algorithm
currently injects a fixed number of dummy events (without attributes) between the first and last
event of the trace. This increases the sampling frequency for the STL subformulas, but clearly
reduces scalability. (Currently, 10.000 events are injected.)
In case of c2, dummy events are injected on the interval where the s1 is at most 0.5, and hence this
is seen by the algorithm. This results in the intuitive result that c2 is not satisfied.
In case of c3, however, the interval where the antecedent holds is too small: no dummy events are
injected in that interval, and hence the algorithm never sees a state where the antecedent of
the implication is true. Therefore, the formula is trivially satisfied.
This clearly is not as expected, but can be explained by the sampling nature of the algorithm for
mixed formulas.
</p>

<p>
The TRACE tool has two features that makes this complicated semantic behavior more transparent.
First, when mixed formulas are specified in the DSL, then a message is shown to the user that
warns for the sampling nature that is used for evaluating the formula.
Second, when the verification algorithm reports the sampling frequency on the console view.
Users can use this to estimate whether the sampling frequency is high enough for their application to
see all relevant changes in the signals.
Figure 4 shows an image of the TRACE UI with the example explained above.
The result view (on the right) shows that c2 is not satisfied and that both c1 and c3 are satisfied.
The specification editor has three "i" indications that warn for the sampling
semantics if the mouse hovers over them.
The console view (bottom) reports the domain on which the verification works, and the
number of injected states and the resulting sampling frequency.
</p>

<figure>
<a target="_blank" href="../img/mixed-ex-2.png"><img class="open600" src="../img/mixed-ex-2.png" border="1"/> </a>
<figcaption>Figure 4: TRACE UI with sampling-semantics features.</figcaption>
</figure>

</div>
</html>