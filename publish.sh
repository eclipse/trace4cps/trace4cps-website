#!/usr/bin/env sh
#
# Copyright (c) 2021 Contributors to the Eclipse Foundation
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#

DIR=${1%/}

[ $# -eq 0 ] && { echo "Usage: $0 dir-name"; exit 1; }
[ ! -d ${DIR} ] && { echo "Directory ${DIR} does not exist."; echo "Usage: $0 dir-name"; exit 1; }

COMMIT_MSG="Set standard visible website to release ${DIR}."
echo ${COMMIT_MSG}

rm -rf $(cat .content)
cp -R ${DIR}/* .
ls ${DIR} > .content
git add -A
git diff-index --quiet HEAD || git commit -s -m "${COMMIT_MSG}"

echo "Please review the last commit before pushing it."